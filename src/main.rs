#![forbid(unsafe_code)]

use std::collections::HashSet;
use std::collections::HashMap;

use std::collections::hash_map::DefaultHasher;

use std::fs::File;

use std::hash::Hasher;

use std::io::ErrorKind;
use std::io::Read;
use std::io::Seek;
use std::io::SeekFrom;

use std::marker::PhantomData;

use std::str::FromStr;

use std::sync::RwLock;

use std::thread;

use lazy_static::lazy_static;

use regex::Regex;

fn main() -> Result<(), std::io::Error> {
    println!("[INFO] -APP:DEVEL:TEST Started...");
    let example_computation = "000B15E2  086732            or [bx+0x32],ah";
    let example_computation_parsed: Instruction = example_computation.parse()?;
    let example_computation_parsed_target = Instruction::ComputeAndWrite(
        CompInstruction {
            op: Op("or".to_owned()),
            sets: vec![
                Reg::PtrSum(
                    vec![
                        RegBase::Named("bx".to_owned()),
                        RegBase::Fixed(0x32_usize as isize)
                    ]
                )
            ].into_iter().collect::<HashSet<_>>(),
            gets: vec![
                Reg::PtrSum(
                    vec![
                        RegBase::Named("bx".to_owned()),
                        RegBase::Fixed(0x32_usize as isize)
                    ]
                ),
                Reg::Base(
                    RegBase::Named("ah".to_owned())
                )
            ].into_iter().collect::<HashSet<_>>(),
        }
    );
    let example_unconditional_jump = "000B15E2  086732            jmp 0x7A";
    let example_unconditional_jump_parsed: Instruction = example_unconditional_jump.parse()?;
    let example_unconditional_jump_parsed_target = Instruction::ControlFlow(
        JumpComponent::Unconditional(
            Reg::Base(
                RegBase::Fixed(0x7A_isize)
            )
        )
    );
    println!("{:?}", example_computation_parsed);
    assert_eq!(example_computation_parsed, example_computation_parsed_target);
    println!("{:?}", example_unconditional_jump_parsed);
    assert_eq!(example_unconditional_jump_parsed, example_unconditional_jump_parsed_target);
    println!("[INFO] -APP:DEVEL:TEST Completed.");
    Ok(())
}

struct CFGDisassembly<'a> {
    disassembly: PreparedDisassembly<'a, FileKnown<'a>>,
    cfg: AssemblyReverseCFG,
}

impl<'a> CFGDisassembly<'a> {
    fn from_disasm(disasm: PreparedDisassembly<'a, FileKnown<'a>>) -> Self {
        CFGDisassembly {
            cfg: AssemblyReverseCFG::build(&disasm),
            disassembly: disasm,
        }
    }
}

impl AssemblyReverseCFG {
    fn build<'a>(disasm: &'a PreparedDisassembly<'a, FileKnown<'a>>) -> Self {
        todo!()
        // Build MultiversalReverseCFG
        // Run mvdiff (AssemblyReverseCFG but without loops)
        // Resolve loops as positions (then return)
    }
}

#[derive(Debug)]
#[derive(Clone)]
struct MultiversalReverseCFG(
    HashMap<usize, HashSet<TargetedMvJumpInfo>> // this allows the use of hmulti_* methods
);

#[derive(Debug)]
#[derive(PartialEq, Eq, Hash, Clone)]
struct TargetedMvJumpInfo {
    in_multiverse: MvDesc,
    from: usize,
}

type MvDesc = Vec<(CondExprForSSA, bool)>;

#[derive(Debug)]
#[derive(Clone)]
struct AssemblyReverseCFG(
    HashMap<usize, SourcesCFG>,
);

#[derive(Debug)]
#[derive(Clone)]
enum SourcesCFG {
    Complex {
        sources: Vec<(CondExprForSSA, usize)>,
    },
    SingleJump {
        target: usize,
    },
    LoopPart {
        loop_positions: LoopPositions,
    },
}

#[derive(Debug)]
#[derive(PartialEq, Eq, Clone)]
enum Instruction {
    ComputeAndWrite(CompInstruction),
    ControlFlow(JumpComponent),
}

macro_rules! incr {
    ($num:ident) => {
        {
            $num = $num + 1;
            $num
        }
    }
}

impl CFGDisassembly<'_> {
    fn backsolve_items(
        &mut self, items: Vec<RegRef>
    ) -> Result<(DerivationMonotonic, Vec<TempAlias>), std::io::Error> {
        todo!(); // still have to add loop stuff
        let mut positions: HashMap<usize, HashSet<SearchForAs>> = HashMap::new();
        let mut derivation: DerivationMonotonic = Vec::new();
        let mut ssa_associations: Vec<TempAlias> = Vec::new();
        let mut alias: TempAlias = 0;
        for i in items {
            let item_associated_alias = incr!(alias);
            positions.hmulti_insert(
                i.location, SearchForAs(i.register, item_associated_alias)
            );
            ssa_associations.push(item_associated_alias);
        }
        // this loop has these associated registers
        let loop_segments: HashMap<LoopPositions, HashSet<SearchForAs>> = HashMap::new();
        loop {
            while !positions.is_empty() {
                let positions_view = positions.clone();
                for (key_index_borrow, value_set) in &positions_view {
                    let key_index = *key_index_borrow;
                    self.disassembly.seek_program(key_index)?;
                    let c_item = self.disassembly.read_program_item()?;
                    for r in value_set {
                        let mut positions_vec: Vec<SearchForAs> = Vec::new();
                        match c_item.clone() {
                            Instruction::ControlFlow(_) => {
                                positions_vec.push(r.clone());
                            },
                            Instruction::ComputeAndWrite(comp_instruction) => {
                                if comp_instruction.sets.contains(&r.0) {
                                    let mut aliases: Vec<TempAlias> = Vec::new();
                                    for g_target in comp_instruction.gets {
                                        let g_alias = incr!(alias);
                                        positions_vec.push(
                                            SearchForAs(g_target, g_alias)
                                        );
                                        aliases.push(g_alias);
                                    }
                                    derivation.push(
                                        MonotonicSSA::Computation {
                                            target: r.1,
                                            f: ExtOp::AsmTransform(
                                                comp_instruction.op
                                            ),
                                            args: aliases,
                                        }
                                    );
                                } else {
                                    positions_vec.push(r.clone());
                                }
                            }
                        }
                        if let Some(control_flow_d) = self.cfg.0.get(&key_index) {
                            match control_flow_d {
                                SourcesCFG::Complex { sources } => {
                                    for i in positions_vec {
                                        let mut possibilities: HashMap<CondExprForSSA, TempAlias> = (
                                            HashMap::new()
                                        );
                                        for (cond_expr, position) in sources {
                                            let case_alias = incr!(alias);
                                            possibilities.insert(cond_expr.clone(), case_alias);
                                            positions.hmulti_insert(
                                                *position,
                                                SearchForAs(i.0.clone(), case_alias)
                                            );
                                        }
                                        derivation.push(
                                            MonotonicSSA::CondSelect {
                                                target: i.1,
                                                possibilities: possibilities,
                                            }
                                        );
                                    }
                                },
                                SourcesCFG::SingleJump { target } => {
                                    for i in positions_vec {
                                        positions.hmulti_insert(target.clone(), i);
                                    }
                                },
                                SourcesCFG::LoopPart { loop_positions } => {
                                    loop_segments.hmulti_insert(*loop_positions, *r);
                                }
                            }
                        } else {
                            for i in positions_vec {
                                positions.hmulti_insert(key_index - 1, i);
                            }
                        }
                        positions.hmulti_remove_part_unchecked_emptiness(&key_index, r);
                    }
                }
                positions.hmulti_clear_invalid_emptiness();
            }
            for (segm, reg_hashset) in loop_segments {
                let (backsolve_result, top_deferred) = self.backsolve_segment_loop(reg_hashset, segm)?;
                derivation.push(
                    MonotonicSSA::LoopSub {
                        inner: Box::new(backsolve_result),
                    }
                );
                for i in top_deferred {
                    positions.hmulti_insert(segm.0, i);
                }
            }
            if positions.is_empty() {
                break;
            }
        }
        Ok((derivation, ssa_associations))
    }

    fn backsolve_segment_loop(
        &mut self, search_targets: HashSet<SearchForAs>, loop_positions: LoopPositions,
    ) -> Result<(DerivationMonotonic, HashSet<SearchForAs>), std::io::Error> {
        todo!()
    }
}

#[derive(Debug)]
#[derive(PartialEq, Eq, Hash, Copy, Clone)]
struct LoopPositions(usize, usize);

#[derive(Debug)]
#[derive(Clone)]
struct LoopAssociationsSSA {
    alpha: HashMap<TempAlias, Reg>,
    beta_enter: Vec<TempAlias>,
    beta_exit: Vec<TempAlias>,
    flag_vreg: TempAlias,
} // (alpha, beta) -> (beta, boolean)

#[derive(Debug)]
#[derive(PartialEq, Eq, Hash, Clone)]
struct SearchForAs(Reg, TempAlias);

type TempAlias = u64;

trait HMultiMapUtils<K: Eq + std::hash::Hash + Copy, V: Eq + std::hash::Hash> {
    fn hmulti_insert(&mut self, _: K, _: V) -> ();
    fn hmulti_clear_invalid_emptiness(&mut self) -> ();
    fn hmulti_remove_part(&mut self, _: &K, _: &V) -> ();
    fn hmulti_remove_part_unchecked_emptiness(&mut self, _: &K, _: &V) -> ();
}

impl<
    K: Eq + std::hash::Hash + Copy, V: Eq + std::hash::Hash
> HMultiMapUtils<K, V> for HashMap<K, HashSet<V>> {
    fn hmulti_insert(&mut self, key: K, value: V) -> () {
        if !self.contains_key(&key) {
            self.insert(key, HashSet::new());
        }
        self.get_mut(&key).expect("Insertion of empty set failed").insert(value);
    }

    fn hmulti_clear_invalid_emptiness(&mut self) -> () {
        let mut keys_empty: Vec<K> = Vec::new();
        for (key, set) in self.iter() {
            if set.is_empty() {
                keys_empty.push(*key);
            }
        }
        for i in keys_empty {
            self.remove(&i);
        }
    }

    fn hmulti_remove_part(&mut self, key: &K, value: &V) -> () {
        let is_part_final;
        match self.get_mut(key) {
            Some(set_ref) => {
                set_ref.remove(value);
                is_part_final = set_ref.is_empty();
            },
            None => {
                is_part_final = false;
                ()
            },
        }
        if is_part_final {
            self.remove(key);
        }
    }

    fn hmulti_remove_part_unchecked_emptiness(&mut self, key: &K, value: &V) -> () {
        match self.get_mut(key) {
            Some(set_ref) => {
                set_ref.remove(value);
            },
            None => (),
        }
    }
}

type DerivationMonotonic = Vec<MonotonicSSA>;

#[derive(Debug)]
#[derive(Clone)]
enum MonotonicSSA {
    Computation {
        target: TempAlias,
        f: ExtOp,
        args: Vec<TempAlias>,
    },
    CondSelect {
        target: TempAlias,
        possibilities: HashMap<CondExprForSSA, TempAlias>,
    },
    LoopSub {
        inner: Box<DerivationMonotonic>,
    }
}

#[derive(Debug)]
#[derive(PartialEq, Eq, Hash, Clone)]
struct CondExprForSSA {
    equal_to: bool,
    op: StdHash,
    gets: Vec<TempAlias>,
}

#[derive(Debug)]
#[derive(Clone)]
enum ExtOp {
    AsmTransform(Op),
    GeneralIdentity,
}

type StdHash = u64;

#[derive(Debug)]
#[derive(PartialEq, Eq, Hash, Clone)]
enum JumpComponent {
    Conditional {
        target: Reg,
        op: StdHash,
        gets: Vec<Reg>,
    },
    Unconditional(Reg),
}

trait ProgramRead {
    type ReadError;
    type ProgramItem;
    fn read_program_item(&mut self) -> Result<Self::ProgramItem, Self::ReadError>;
}

trait ProgramSeek: ProgramRead {
    type SeekError;
    fn seek_program(&mut self, _: usize) -> Result<(), Self::SeekError>;
}

impl ProgramRead for PreparedDisassembly<'_, FileKnown<'_>> {
    type ReadError = std::io::Error;
    type ProgramItem = Instruction;
    fn read_program_item(&mut self) -> Result<Self::ProgramItem, Self::ReadError> {
        let mut buf: Vec<u8> = Vec::new();
        loop {
            let mut curr_item = [0x00_u8; 1];
            self.fpath.file.read_exact(&mut curr_item)?;
            let curr_byte = curr_item[0];
            if curr_byte == 0x0A_u8 {
                break;
            } else {
                buf.push(curr_byte);
            }
        }
        let text = from_utf8_io_result(buf)?;
        text.parse()
    }
}

fn from_utf8_io_result(vec: Vec<u8>) -> Result<String, std::io::Error> {
    match std::str::from_utf8(&vec) {
        Ok(x) => Ok(x.to_owned()),
        Err(utf8_err) => Err(
            std::io::Error::new(ErrorKind::InvalidData, format!("{}", utf8_err))
        ),
    }
}

impl FromStr for Instruction {
    type Err = std::io::Error;
    fn from_str(s: &str) -> Result<Self, <Self as FromStr>::Err> {
        let instruction_part = &s[28..];
        let ws_split_vec = instruction_part
            .split_whitespace()
            .collect::<Vec<_>>();
        let (instr, ws_split_remaining) = ws_split_vec.split_at(1);
        if instr[0] == "jmp" {
            return Ok(
                Instruction::ControlFlow(
                    JumpComponent::Unconditional(
                        ws_split_remaining
                            .into_iter()
                            .next()
                            .ok_or(
                                std::io::Error::new(
                                    ErrorKind::InvalidData,
                                    "Unconditional jump (jmp) with no target"
                                )
                            )?
                            .parse()?
                    )
                )
            );
        }
        let instr_op = Op(instr[0].to_owned());
        let remaining = ws_split_remaining.join("");
        let mut operands = remaining.split(",");
        if let Some(args) = instr_op.conditional_jump() {
            return Ok(
                Instruction::ControlFlow(
                    JumpComponent::Conditional {
                        target: {
                            operands
                                .next()
                                .expect("Conditional jump with no target")
                                .parse()?
                        },
                        op: {
                            let mut hasher = DefaultHasher::new();
                            hasher.write(instr_op.0.as_bytes());
                            hasher.finish()
                        },
                        gets: {
                            let mut gets: Vec<Reg> = Vec::new();
                            gets.extend(args);
                            for i in operands {
                                gets.push(i.parse()?);
                            }
                            gets
                        },
                    }
                )
            );
        }
        let remaining = ws_split_remaining.join("");
        let mut operands = remaining.split(",").peekable();
        let mut instruction_sets = HashSet::new();
        let mut instruction_gets = HashSet::new();
        match operands.peek() {
            Some(x) => {
                instruction_sets.insert(x.parse()?);
                ()
            },
            None => {
                ()
            },
        };
        for i in operands {
            instruction_gets.insert(i.parse()?);
        }
        Ok(
            Instruction::ComputeAndWrite(
                CompInstruction {
                    op: Op(instr[0].to_owned()),
                    sets: instruction_sets,
                    gets: instruction_gets,
                }
            )
        )
    }
}

impl Op {
    fn conditional_jump(&self) -> Option<Vec<Reg>> {
        return None; // TODO impl
        todo!()
    }
}

impl FromStr for CompInstruction {
    type Err = std::io::Error;
    fn from_str(s: &str) -> Result<Self, <Self as FromStr>::Err> {
        let instruction_part = &s[28..];
        let ws_split_vec = instruction_part
            .split_whitespace()
            .collect::<Vec<_>>();
        let (instr, ws_split_remaining) = ws_split_vec.split_at(1);
        let remaining = ws_split_remaining.join("");
        let mut operands = remaining.split(",").peekable();
        let mut instruction_sets = HashSet::new();
        let mut instruction_gets = HashSet::new();
        match operands.peek() {
            Some(x) => {
                instruction_sets.insert(x.parse()?);
                ()
            },
            None => {
                ()
            },
        };
        for i in operands {
            instruction_gets.insert(i.parse()?);
        }
        Ok(
            CompInstruction {
                op: Op(instr[0].to_owned()),
                sets: instruction_sets,
                gets: instruction_gets,
            }
        )
    }
}

impl FromStr for Reg {
    type Err = std::io::Error;
    fn from_str(s: &str) -> Result<Self, <Self as FromStr>::Err> {
        lazy_static! {
            static ref REGEX_PTR_SUM: Regex = Regex::new(
                r#"\[\w+([\+\-]\w+)*\]"#
            ).unwrap();
        }
        Ok(
            if s.chars().all(|c| c.is_alphanumeric()) {
                Reg::Base(s.parse()?)
            } else {
                if REGEX_PTR_SUM.is_match(s) {
                    let corrected_sum_expr = (&s[1..(s.len() - 1)])
                        .replace("-", "-~");
                    let reg_base_parts = corrected_sum_expr
                        .split(&['+', '-'][..]);
                    let mut sum_definition: Vec<RegBase> = Vec::new();
                    for i in reg_base_parts {
                        sum_definition.push(i.parse()?);
                    }
                    Reg::PtrSum(sum_definition)
                } else {
                    let components = s
                        .split(|c: char| !c.is_alphanumeric())
                        .filter(|x| *x != "");
                    let mut components_parsed: Vec<RegBase> = Vec::new();
                    for i in components {
                        components_parsed.push(i.parse()?)
                    }
                    Reg::PtrArithOther(components_parsed)
                }
            }
        )
    }
}

impl FromStr for RegBase {
    type Err = std::io::Error;
    fn from_str(s: &str) -> Result<Self, <Self as FromStr>::Err> {
        let empty_error = std::io::Error::new(ErrorKind::InvalidInput, "Cannot parse \"\" as RegBase");
        Ok(
            if s.chars().next().ok_or(empty_error)?.is_numeric() {
                RegBase::Fixed(parse_general_isize(s)?)
            } else {
                RegBase::Named(s.to_owned())
            }
        )
    }
}

fn parse_general_isize(s: &str) -> Result<isize, std::io::Error> {
    let empty_error = std::io::Error::new(ErrorKind::InvalidInput, "Cannot parse \"\" as any isize");
    let mut char_iter = s.chars();
    let first_char = char_iter.next().ok_or(empty_error)?;
    if first_char == '~' {
        Ok(
            -parse_general_isize(&s[1..])?
        )
    } else {
        Ok(
            parse_general_usize_informed(s, first_char, char_iter)? as isize
        )
    }
}

fn parse_general_usize(s: &str) -> Result<usize, std::io::Error> {
    let empty_error = std::io::Error::new(ErrorKind::InvalidInput, "Cannot parse \"\" as any isize");
    let mut char_iter = s.chars();
    let first_char = char_iter.next().ok_or(empty_error)?;
    parse_general_usize_informed(s, first_char, char_iter)
}

fn parse_general_usize_informed(
    s: &str,
    first_char: char,
    mut char_iter: impl Iterator<Item = char>
) -> Result<usize, std::io::Error> {
    if first_char != '0' {
        match s.parse() {
            Ok(x) => Ok(x),
            Err(parse_error) => Err(
                std::io::Error::new(
                    ErrorKind::InvalidData,
                    format!("{}", parse_error)
                )
            )
        }
    } else {
        Ok(
            match char_iter.next() {
                Some('x') => usize::from_str_radix_io(&s[2..], 16)?,
                Some('b') => usize::from_str_radix_io(&s[2..], 2)?,
                Some(..) => usize::from_str_radix_io(s, 8)?,
                None => 0x0_usize,
            }
        )
    }
}

trait FromStrRadixIO where Self: Sized {
    fn from_str_radix_io(src: &str, radix: u32) -> Result<Self, std::io::Error>;
}

impl FromStrRadixIO for usize {
    fn from_str_radix_io(src: &str, radix: u32) -> Result<Self, std::io::Error> {
        match Self::from_str_radix(src, radix) {
            Ok(x) => Ok(x),
            Err(parse_error) => Err(
                std::io::Error::new(ErrorKind::InvalidData, format!("{}", parse_error))
            ),
        }
    }
}

impl FromStrRadixIO for isize {
    fn from_str_radix_io(src: &str, radix: u32) -> Result<Self, std::io::Error> {
        Ok(
            usize::from_str_radix_io(src, radix)? as isize
        )
    }
}

#[derive(Debug)]
#[derive(PartialEq, Eq, Clone)]
struct CompInstruction {
    op: Op,
    sets: HashSet<Reg>,
    gets: HashSet<Reg>,
}

impl ProgramSeek for PreparedDisassembly<'_, FileKnown<'_>> {
    type SeekError = std::io::Error;
    fn seek_program(&mut self, target: usize) -> Result<(), Self::SeekError> {
        let target_bytes = self.locations
            .get(&target)
            .ok_or(
                std::io::Error::new(ErrorKind::NotFound, "Could not find usize")
            )?;
        self.fpath.file.seek(
            SeekFrom::Start(*target_bytes)
        )?;
        Ok(())
    }
}

#[derive(Debug)]
#[derive(Clone)]
struct PreparedDisassembly<'a, R = &'a str> where R: FileRef {
    fpath: R,
    locations: DisassemblyLocationMap,
    phantom: PhantomData<&'a str>,
}

type DisassemblyLocationMap = HashMap<usize, u64>;

impl<'a> PreparedDisassembly<'a> {
    fn try_new(fpath: &'a str) -> Result<Self, std::io::Error> {
        let locations = derive_file_locations(fpath)?;
        Ok(
            Self {
                fpath,
                locations,
                phantom: PhantomData,
            }
        )
    }
}

impl<'a> PreparedDisassembly<'a> {
    fn try_upgrade(&'a self) -> Result<PreparedDisassembly<'a, FileKnown<'a>>, std::io::Error> {
        let file_known = self.fpath.refers_to_file()?;
        Ok(
            PreparedDisassembly::<'a, FileKnown<'a>> {
                fpath: file_known,
                locations: self.locations.clone(),
                phantom: PhantomData,
            }
        )
    }
}

fn derive_file_locations(fpath: &str) -> Result<DisassemblyLocationMap, std::io::Error> {
    let mut locations: DisassemblyLocationMap = HashMap::new();
    let mut reader = reader_noalloc::BufReader::open(fpath)?;
    let mut buffer = String::new();
    let mut last_index = 0_u64;
    while let Some(line) = reader.read_line(&mut buffer).transpose()? {
        let index_word: &str = &line[..8];
        let assembly_index = usize::from_str_radix_io(index_word, 16_u32)?;
        locations.insert(assembly_index, last_index);
        last_index = reader.get_position_bytes();
    }
    Ok(locations)
}

mod reader_noalloc {
    use std::fs::File;
    use std::io;
    use std::io::prelude::*;

    pub struct BufReader {
        reader: io::BufReader<File>,
    }

    impl BufReader {
        pub fn open(path: impl AsRef<std::path::Path>) -> io::Result<Self> {
            let file = File::open(path)?;
            let reader = io::BufReader::new(file);
            Ok(
                Self { reader }
            )
        }

        pub fn read_line<'buf>(
            &mut self,
            buffer: &'buf mut String,
        ) -> Option<io::Result<&'buf mut String>> {
            buffer.clear();
            self.reader
                .read_line(buffer)
                .map(|u| if u == 0 { None } else { Some(buffer) })
                .transpose()
        }

        pub fn get_position_bytes(&mut self) -> u64 {
            self.reader.seek(io::SeekFrom::Current(0_i64)).expect("No-op seek failed")
        }
    }
}

trait FileRef {
    fn refers_to_file(&self) -> Result<FileKnown, std::io::Error>;
    fn refers_to_file_at(&self) -> &str;
}

impl FileRef for String {
    fn refers_to_file(&self) -> Result<FileKnown, std::io::Error> {
        let fh = File::open(&self)?;
        Ok(
            FileKnown {
                fpath: &self,
                file: fh,
            }
        )
    }

    fn refers_to_file_at(&self) -> &str {
        &self
    }
}

impl FileRef for &str {
    fn refers_to_file(&self) -> Result<FileKnown, std::io::Error> {
        let fh = File::open(&self)?;
        Ok(
            FileKnown {
                fpath: &self,
                file: fh,
            }
        )
    }

    fn refers_to_file_at(&self) -> &str {
        &self
    }
}

impl FileRef for FileKnown<'_> {
    fn refers_to_file(&self) -> Result<FileKnown, std::io::Error> {
        let fh = File::open(&self.fpath)?;
        Ok(
            FileKnown {
                fpath: &self.fpath,
                file: fh,
            }
        )
    }

    fn refers_to_file_at(&self) -> &str {
        &self.fpath
    }
}

#[derive(Debug)]
struct FileKnown<'a> {
    fpath: &'a str,
    file: File,
}

#[derive(Debug)]
#[derive(PartialEq, Eq, Hash, Clone)]
enum Reg {
    Base(RegBase),
    PtrSum(Vec<RegBase>),
    PtrArithOther(Vec<RegBase>),
}

#[derive(Debug)]
#[derive(PartialEq, Eq, Hash, Clone)]
enum RegBase {
    Named(String),
    Fixed(isize),
}

#[derive(Debug)]
#[derive(Clone)]
struct RegRef {
    register: Reg,
    location: usize,
}

#[derive(Debug)]
#[derive(PartialEq, Eq, Clone)]
struct Op(String);

