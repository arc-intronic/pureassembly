#include<iostream>

void greet(bool wld) {
	if (wld) {
		std::cout << "Hello, world!" << "\n";
	} else {
		std::cout << "Hello!" << "\n";
	}
}

int main() {
	std::cout << "begin" << "\n";
	greet(true);
	std::cout << "end" << "\n";
}

