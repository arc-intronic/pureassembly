# TODO (Project Components)

- [x] Assembly Parsing
- [X] Index Translation Map **UNTESTED**
- [ ] GOTO Solver (AssemblyCFG)
- [ ] Multiversal Difference (mvdiff)
- [ ] Monotonic SSA Backsolving **PROG**
- [ ] Derivation Export (MSGPACK-C1)
- [ ] Monadic Invocation **REQUIRE_FINAL**
